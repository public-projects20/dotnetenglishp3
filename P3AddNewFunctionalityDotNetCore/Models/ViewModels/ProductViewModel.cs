﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace P3AddNewFunctionalityDotNetCore.Models.ViewModels
{
    /// <summary>
    /// if any reuired property is null -> set IsValidState to false
    /// </summary>
    public class ProductViewModel : IValidatableObject
    {
        [BindNever]
        public int Id { get; set; }

        [Required(ErrorMessage = "{0} must have a value.")]
        [Display(Name = "Product's name")]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Details { get; set; }

        [Required(ErrorMessage = "{0} must have a value.")]
        [Display(Name = "Product's stock")]
        public string Stock { get; set; }

        [Required(ErrorMessage = "{0} must have a value.")]
        [Display(Name = "Product's price")]
        public string Price { get; set; }

        /// <summary>
        /// return error and set IsValidState to false (if invalid value exists)
        /// </summary>
        /// <param name="validationContext"></param>
        /// <returns></returns>
        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            var results = new List<ValidationResult>();

            bool isValidStock = Int32.TryParse(Stock, out _);
            if (!isValidStock)
                results.Add(new ValidationResult($"The value of Stock is not valid: {Stock}. Please fill a integer value"));

            bool isValidPrice = decimal.TryParse(Price, out _);
            if (!isValidPrice)
                results.Add(new ValidationResult($"The value of Price is not valid: {Stock}. Please fill a decimal value"));

            return results;
        }
    }
}
