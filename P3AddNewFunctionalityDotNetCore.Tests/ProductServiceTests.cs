﻿using P3AddNewFunctionalityDotNetCore.Models.Entities;
using P3AddNewFunctionalityDotNetCore.Models.Services;
using P3AddNewFunctionalityDotNetCore.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Xunit;
using Xunit.Sdk;

namespace P3AddNewFunctionalityDotNetCore.Tests
{
    public class ProductServiceTests : BaseTest
    {
        private readonly IProductService _productService;

        public ProductServiceTests()
        {
            _productService = this.GetRequiredService<IProductService>();
        }
        /// <summary>
        /// Take this test method as a template to write your test method.
        /// A test method must check if a definite method does its job:
        /// returns an expected value from a particular set of parameters
        /// </summary>
        /// 
        [Fact, TestPriority(0)]
        public void GetAllProductsViewModel()
        {
            try
            {
                IEnumerable<ProductViewModel> products = _productService.GetAllProductsViewModel();
                Assert.NotNull(products);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message);
            }
        }

        [Fact, TestPriority(1)]
        public void GetAllProducts()
        {
            try
            {
                IEnumerable<Product> products = _productService.GetAllProducts();
                if (products == null)
                {
                    Assert.NotNull(products);
                }
                else
                {
                    Assert.True(products.Any(), "The collection of product is empty, please check the Database");
                }
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message);
            }
        }

        [Fact, TestPriority(2)]
        public void GetProductByIdViewModel()
        {
            try
            {
                IEnumerable<Product> products = _productService.GetAllProducts();
                int _productId = 0;
                if (products == null || !products.Any())
                {
                    Assert.True(products.Any(), "The collection of product is empty, please check the Database");
                }
                _productId = products.FirstOrDefault().Id;

                ProductViewModel productViewModel = _productService.GetProductByIdViewModel(_productId);
                Assert.Equal(1, 1);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message);
            }
        }

        [Fact, TestPriority(3)]
        public void GetProductById()
        {
            try
            {
                IEnumerable<Product> products = _productService.GetAllProducts();
                int _productId = 0;
                if (products == null || !products.Any())
                {
                    Assert.True(products.Any(), "The collection of product is empty, please check the Database");
                }
                _productId = products.FirstOrDefault().Id;

                Product product = _productService.GetProductById(_productId);
                Assert.Equal(1, 1);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message);
            }
        }

        [Fact, TestPriority(4)]
        public async Task GetProduct()
        {
            try
            {
                IEnumerable<Product> products = _productService.GetAllProducts();
                int _productId = 0;
                if (products == null || !products.Any())
                {
                    Assert.True(products.Any(), "The collection of product is empty, please check the Database");
                }
                _productId = products.FirstOrDefault().Id;

                Product product = await _productService.GetProduct(_productId);
                Assert.Equal(1, 1);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message);
            }
        }

        [Fact, TestPriority(5)]
        public async Task GetProducts()
        {
            try
            {
                IList<Product> products = await _productService.GetProduct();
                Assert.Equal(1, 1);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message);
            }
        }

        [Fact, TestPriority(6)]
        public void UpdateProductQuantities()
        {
            try
            {
                _productService.UpdateProductQuantities();
                Assert.Equal(1, 1);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message);
            }
        }

        [Fact, TestPriority(7)]
        public void SaveProduct()
        {
            try
            {
                var product = new ProductViewModel
                {
                    Name = "test",
                    Description = "test",
                    Details = "test",
                    Stock = "1",
                    Price = "1",
                };
                _productService.SaveProduct(product);

                Assert.Equal(1, 1);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message);
            }
        }

        [Fact, TestPriority(8)]
        public void DeleteProduct()
        {
            try
            {
                IEnumerable<Product> products = _productService.GetAllProducts();
                int _productId = 0;
                if (products == null || !products.Any())
                {
                    Assert.True(products.Any(), "The collection of product is empty, please check the Database");
                }
                _productId = products.FirstOrDefault().Id;

                _productService.DeleteProduct(_productId);

                Product product = _productService.GetProductById(_productId);
                Assert.Null(product);
            }
            catch (Exception ex)
            {
                Assert.True(false, ex.Message);
            }
        }
    }
}